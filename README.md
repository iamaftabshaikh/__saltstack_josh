# Saltstack for Josh

- [] Install software
- [] Deploy minion and accept into SSE
- [] Complete basic configuration
- [] State files to patch OS
- [] Configure win_repo
- [] Configure chocolatey
- [] State file to patch Adobe Reader (win_repo)
- [] State file to patch Oracle Java (chocolatey)
- [] Orchestration to upgrade postgresdb
- [] Create custom benchmark + one check
- [] Integration with Qualys (import file from qualys vulnerability scanner)
- [] State file to install firmware tools to OS
- [] Write custom discovery grain for firmware
