# -*- coding: utf-8 -*-
# vim: ft=sls

{% from (tpldir + "/map.jinja") import java_settings with context %}

install_oracle_jdk:
  pkg.installed:
    - name: {{ java_settings.common.package }}
    - version: {{ java_settings.common.version }}
    - refresh: true

environment_jdk_config:
  file.managed:
    - name: {{ java_settings.config.filename }}
    - source: {{ java_settings.config.source }}
    - user: root
    - group: root
    - mode: 644
    - template: jinja

{% if java_settings.public_store.store_pass %}
pubstore_passwd_chage:
  cmd.run:
    - name: keytool -storepasswd -new {{ java_settings.public_store.store_pass }} -storepass {{ java_settings.public_store.default_store_pass }} -keystore {{ java_settings.public_store.path }}
    - unless:
      - keytool -storepass {{ java_settings.public_store.store_pass }} -list -keystore {{ java_settings.public_store.path }}
{% endif %}
