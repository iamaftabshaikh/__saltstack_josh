# -*- coding: utf-8 -*-
# vim: ft=sls

{% from (tpldir + "/map.jinja") import java_settings with context %}

{% set nexus_server = "slitrepo.it.slb.com:8443" %}


setup_java_repo:
  pkgrepo.managed:
    - name: {{ java_settings.repo.name }}
    - humanname: Java Command Tools
    - baseurl: https://{{ nexus_server }}/repository/datalake-pond-repo-rpm/java/el7/
    - gpgcheck: 0
    - enabled: true
    - comments:
      - "JDK repository - Managed by SaltStack, DO NOT edit by hand."
