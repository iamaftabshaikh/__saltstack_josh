# -*- coding: utf-8 -*-
# vim: ft=sls

{% from (tpldir + "/map.jinja") import java_settings with context %}

include:
  - {{ slspath }}/repo
  - {{ slspath }}/common
  - {{ slspath }}/jce
