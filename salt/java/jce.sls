# -*- coding: utf-8 -*-
# vim: ft=sls

{% from (tpldir + "/map.jinja") import java_settings with context %}

{% if java_settings.jce.enable %}
java_jce_enable:
  file.replace:
    - name: /usr/java/latest/jre/lib/security/java.security
    - pattern: ^#crypto.policy=unlimited
    - repl: crypto.policy=unlimited
    - backup: false

{% else %}
java_jce_disable:
  file.replace:
    - name: /usr/java/latest/jre/lib/security/java.security
    - pattern: ^crypto.policy=unlimited
    - repl: '#crypto.policy=unlimited'
    - backup: false

{% endif %}

